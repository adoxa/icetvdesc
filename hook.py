from Plugins.SystemPlugins.IceTV.plugin import EPGFetcher
from RecordTimer import RecordTimerEntry
from re import search

from Screens.Screen import Screen
from Components.ConfigList import ConfigListScreen
from Components.config import config, getConfigListEntry, ConfigSubsection, ConfigYesNo
from Components.ActionMap import ActionMap
from Components.Label import Label
from Screens.MessageBox import MessageBox
from enigma import eEPGCache

try:
	from Plugins.Extensions.Series2Folder.plugin import Series2FolderActionsBase
	from os.path import join as joinpath, splitext
	from time import localtime, strftime
	S2F = True
except:
	S2F = False

from Plugins.Plugin import PluginDescriptor
from functools import wraps

__version__ = "1.0.7.2"

config.plugins.icetvdesc = ConfigSubsection()
config.plugins.icetvdesc.extradesc = ConfigYesNo(default=True)
config.plugins.icetvdesc.numbers = ConfigYesNo(default=False)
config.plugins.icetvdesc.titlenumbers = ConfigYesNo(default=False)
config.plugins.icetvdesc.titleseason = ConfigYesNo(default=False)
config.plugins.icetvdesc.seasonfolders = ConfigYesNo(default=False)


def hook(oldfunction, newfunction):
	@wraps(oldfunction)
	def run(self, *args, **kwargs):
		return newfunction(self, oldfunction, *args, **kwargs)
	run.unhook = oldfunction
	return run


def pad(val, fill="0"):
	if len(val) == 1:
		val = fill + val
	return val


def convertChanShows(self, oldfunction, shows, mapping_errors):
	for show in shows:
		if config.plugins.icetvdesc.extradesc.value:
			extended = show.get("desc", "").rstrip().replace("\r", "") + "\n"
			year = show.get("date", "0")
			if year != "0":
				title = show.get("title", "")
				if title.startswith("Movie: "):
					title += " (%s)" % year
					show["title"] = title
					try:
						director = show["credits"]["director"]
						extended += "\nDirector: " + director
					except:
						pass
				else:
					extended += "\nYear: " + year
			try:
				cast = ", ".join([actor["name"] for actor in show["credits"]["actors"]])
				if cast:
					extended += "\nCast: " + cast
			except:
				pass
			show["desc"] = extended

		if config.plugins.icetvdesc.numbers.value \
				and not search(r"(?:Series|Season) +[\d/]+ *. *(?:Episode|Part) +\d+ *$", show["subtitle"]):
			season = show.get("season")
			episode = show.get("episode-num")
			number = ""
			if season:
				number = "S" + pad(season)
			if episode:
				number += "E" + pad(episode)
			if number:
				show["subtitle"] += " (%s)" % number

		if config.plugins.icetvdesc.titleseason.value:
			season = show.get("season")
			if not season:
				found = search(r"(?:Series|Season) +([\d/]+)", show["subtitle"])
				if found:
					season = found.group(1)
			if season:
				show["title"] += " (Season %s)" % pad(season, " ")

	return oldfunction(self, shows, mapping_errors)


def calculateFilename(self, oldfunction, name=None):
	if config.plugins.icetvdesc.titlenumbers.value:
		name = name or self.name
		if config.plugins.icetvdesc.titleseason.value:
			found = search(r" \(Season  ?[\d/]+\)$", name)
			if found:
				name = name[:found.start()]
		desc = self.description
		number = None
		found = search(r" \((?=[SE])((?:S\d+)?(?:E\d+)?)\)$", desc)
		if found:
			number = found.group(1)
		else:
			found = search(r":? *(?:Series|Season) +([\d/]+) *. *(?:Episode|Part) +(\d+) *$", desc)
			if found:
				season = pad(found.group(1))
				episode = pad(found.group(2))
				number = "S%sE%s" % (season, episode)
		if number:
			if config.recording.filename_composition.value != "long":
				name += " - " + number
			else:
				self.description = desc[:-len(found.group(0))]
				if not self.description:
					self.description = number
				else:
					name += " - " + number
				name = oldfunction(self, name)
				self.description = desc
				return name
	return oldfunction(self, name)


# A duplicate of Series2Folder, but allowing '/'.
def cleanName(self, oldfunction, name):
	name = name.strip()

	if not self.conf_portablenames:
		return name

	# filter out non-allowed characters
	non_allowed_characters = ".\\:*?<>|\""
	name = name.replace('\xc2\x86', '').replace('\xc2\x87', '')
	name = ''.join(['_' if c in non_allowed_characters or ord(c) < 32 else c for c in name])
	return name

# A duplicate of Series2Folder, adding season folder.
def getShowInfo(self, oldfunction, rootdir, fullname):
	path = joinpath(rootdir, fullname) + self.META
	err_mess = None
	try:
		lines = open(path).readlines()
		showname = lines[1].strip()
		desc = lines[2].strip()
		t = int(lines[3].strip())
		pending_merge = len(lines) > 4 and "pts_merge" in lines[4].strip().split(' ')
		date_time = strftime("%d.%m.%Y %H:%M", localtime(t))
		filebase = splitext(fullname)[0]
		if filebase[-4:-3] == "_" and filebase[-3:].isdigit():
			date_time += '#' + filebase[-3:]
	except:
		showname, date_time, pending_merge, err_mess = self.recSplit(fullname)
		desc = ''

	if showname:
		showname = showname.replace('/', '_')
		showname = showname[:255]
		if config.plugins.icetvdesc.seasonfolders.value:
			found = search(r" \((Season  ?[\d_]+)\)$", showname)
			if found:
				showname = showname[:found.start()] + "/" + found.group(1)
			else:
				found = search(r" \(S(\d+)(?:E\d+)?\)$", desc)
				if found:
					number = found.group(1)
					if number.startswith('0'):
						number = ' ' + number[1:]
				else:
					found = search(r":? *(?:Series|Season) +([\d/]+) *. *(?:Episode|Part) +(\d+) *$", desc)
					if found:
						number = pad(found.group(1)).replace('/', '_')
				if found:
					showname += "/Season " + number

	return showname, pending_merge, date_time, err_mess


class IceTVDescConfig(ConfigListScreen, Screen):
	skin = """
	<screen position="center,center" size="600,300" title="IceTVDesc" >
		<widget name="config" position="10,10" size="e-10,e-120" scrollbarMode="showOnDemand" />
		<widget name="description" position="10,e-110" size="e-20,60" font="Regular;18" foregroundColor="grey" halign="left" valign="top" />
		<ePixmap position="20,e-25" size="15,16" valign="center" pixmap="skin_default/buttons/button_red.png" alphatest="blend" />
		<ePixmap position="170,e-25" size="15,16" valign="center" pixmap="skin_default/buttons/button_green.png" alphatest="blend" />
		<widget name="key_red" position="40,e-30" size="150,25" valign="center" halign="left" font="Regular;20" />
		<widget name="key_green" position="190,e-30" size="150,25" valign="center" halign="left" font="Regular;20" />
	</screen>"""

	def __init__(self,session):
		self.session = session
		Screen.__init__(self, session)

		entries = [
			getConfigListEntry(
				_("Add year/cast to extended description"),
				config.plugins.icetvdesc.extradesc,
				_('For a movie the year will be appended to the title ("Movie: Title (Year)") '
				  'and the director will be added to the description.')
			),
			getConfigListEntry(
				_("Add season/episode numbers to short description"),
				config.plugins.icetvdesc.numbers,
				_('If the episode title doesn\'t already show the season (or series) and '
				  'episode (or part) numbers append them ("Episode Title (SnnEnn)").')
			),
			getConfigListEntry(
				_("Add season/episode numbers to file name"),
				config.plugins.icetvdesc.titlenumbers,
				_('If season/episode numbers are present in the short description they\'ll be '
				  'added after the title ("... - Title - SnnEnn.ts" or "... - Title - SnnEnn - '
				  'Episode Title.ts").')
			),
			getConfigListEntry(
				_("Add season to title"),
				config.plugins.icetvdesc.titleseason,
				_('Add the season to the title ("Title (Season  n)").  It will be removed from '
				  'the file name if the above option is also used.')
			),
		]
		if S2F:
			entries.append(getConfigListEntry(
				_("Series to season folders"),
				config.plugins.icetvdesc.seasonfolders,
				_('Series to Folder will create subfolders for each season (if the number is '
				  'present in the title or description).')
			))
		ConfigListScreen.__init__(self, entries, session=self.session)

		self["description"] = Label()
		self["key_red"] = Label(_("Cancel"))
		self["key_green"] = Label(_("Save"))
		self["setupActions"] = ActionMap(["SetupActions", "ColorActions"], {
			"ok": self.keySave,
			"cancel": self.keyCancel,
			"green": self.keySave,
			"red": self.keyCancel,
		}, -2)
			
		self.setTitle(_("IceTVDesc - v%s") % __version__)

	def keySave(self):
		config.plugins.icetvdesc.titlenumbers.save()
		config.plugins.icetvdesc.seasonfolders.save()
		if self["config"].isChanged():
			self.saveAll()
			self.session.openWithCallback(self.resetEPG, MessageBox,
				_('Settings will only be applied to new EPG entries, '
				  'unless the EPG is reset.  Reset now?'), default=False)
		else:
			self.close()

	def resetEPG(self, answer):
		if answer:
			epgcache = eEPGCache.getInstance()
			epgcache.clear()
			epgcache.save()
			try:
				config.plugins.icetv.last_update_time.value = 0
				from Plugins.SystemPlugins.IceTV.plugin import IceTVMain
				IceTVMain(self.session).fetch()
			except:
				pass
		self.close()


def autostart(reason, **kwargs):
	if reason == 0: # startup
		EPGFetcher.convertChanShows = hook(EPGFetcher.convertChanShows, convertChanShows)
		RecordTimerEntry.calculateFilename = hook(RecordTimerEntry.calculateFilename, calculateFilename)
		if S2F:
			Series2FolderActionsBase.cleanName = hook(Series2FolderActionsBase.cleanName, cleanName)
			Series2FolderActionsBase.getShowInfo = hook(Series2FolderActionsBase.getShowInfo, getShowInfo)
	elif reason == 1: # shutdown
		EPGFetcher.convertChanShows = EPGFetcher.convertChanShows.unhook
		RecordTimerEntry.calculateFilename = RecordTimerEntry.calculateFilename.unhook
		if S2F:
			Series2FolderActionsBase.cleanName = Series2FolderActionsBase.cleanName.unhook
			Series2FolderActionsBase.getShowInfo = Series2FolderActionsBase.getShowInfo.unhook

def menustart(session, **kwargs):
	session.open(IceTVDescConfig)

plugin = [
	PluginDescriptor(where=[PluginDescriptor.WHERE_AUTOSTART], fnc=autostart),
	PluginDescriptor(name="IceTVDesc Setup", description=_("Incorporate additional IceTV data into the extended description"), where=[PluginDescriptor.WHERE_PLUGINMENU], fnc=menustart)
]
